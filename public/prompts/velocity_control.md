# Task

Reach the end of the maze with velocity (0.0, 2.4)


***

# Synopsis

In this level, we will guide you in implementing a **Proportional Feedback Controller (P-controller)**.

The provided skeleton code contains the methods that are needed to implement a P-controller.

Goodluck!


