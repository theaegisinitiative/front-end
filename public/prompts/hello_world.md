# Task

**Reach the exit of the maze**

***

# Synopsis

Welcome to Robobot!

This module will aim to introduce you to **Proportional-Integral-Derivative (PID) Controllers**. PID Controllers are a type of feedback controllers widely used in the real world to accurately control systems. You will likely find PID controllers to be very useful when building your robot for our competitions.

To get started, change your robot's acceleration in the code editor on the right to reach the exit.

Goodluck!

