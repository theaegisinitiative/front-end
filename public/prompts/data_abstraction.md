# Task

**Reach the end of the maze by completing the skeleton code provided!**

***

# Synopsis
This tutorial will help familiarize you with the data structure your Robobot uses to store sensory information it collects.

To get you started, we provided skeleton code for the implementation of the following accessor methods for commonly used data
- `cur_position(self)`
- `prev_position(self)`
- `cur_delta_time(self)`

In the future, you may build your own data abstractions to keep your robot tractable an scalable as your robot becomes increasingly more complex and powerful.
