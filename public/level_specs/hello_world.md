# Help
Acceleration is a vector (x, y) where x is left-right and y is up-down.

Try:
```
self.set_acceleration ((0, 1))
```
This will give your robot an upwards acceleration of 1 $units/second^2$.
